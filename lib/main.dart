import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(child: Column(),),
      body: Container(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              centerTitle: true,
              title: Text('AppBar'),
              floating: true,
              expandedHeight: 100.0,
            ),
            SliverToBoxAdapter(
              child: Container(
                color: Colors.green,
                height: 400,
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                color: Colors.amber,
                height: 400,
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                color: Colors.orange,
                height: 400,
              ),
            ),
            SliverToBoxAdapter(
              child: Container(
                color: Colors.red,
                height: 400,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
